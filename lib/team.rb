require 'yaml'

module Gitlab
  module Homepage
    class Team
      def members
        @members ||= Team::Member.all! do |member|
          Team::Project.all! do |project|
            member.assign(project) if member.involved?(project)
          end
        end
      end

      def projects
        @projects ||= Team::Project.all! do |project|
          Team::Member.all! do |member|
            project.assign(member) if member.involved?(project)
          end
        end
      end

      def departments
        @departments ||= begin
          Hash.new(0).tap do |departments|
            Team::Member.all! do |member|
              member.departments.each { |department| departments[department] += 1 }
            end
          end
        end
      end
    end
  end
end
